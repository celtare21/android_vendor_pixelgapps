# Copyright (C) 2017 The Pure Nexus Project
# Copyright (C) 2018-2019 The Dirty Unicorns Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

ifneq ($(filter product system/product,$(TARGET_COPY_OUT_PRODUCT)),)
  TARGET_COPY_GAPPS_OUT := $(get_build_var TARGET_COPY_OUT_SYSTEM)
  product_bool := false
  copyoutblobs := system
else
  TARGET_COPY_GAPPS_OUT := $(get_build_var TARGET_COPY_OUT_PRODUCT)
  product_bool := true
  copyoutblobs := product
endif

PRODUCT_COPY_FILES += \
    vendor/pixelgapps/etc/default-permissions/default-permissions.xml:$(copyoutblobs)/etc/default-permissions/default-permissions.xml \
    vendor/pixelgapps/etc/permissions/com.google.android.dialer.support.xml:$(copyoutblobs)/etc/permissions/com.google.android.dialer.support.xml \
    vendor/pixelgapps/etc/permissions/com.google.android.maps.xml:$(copyoutblobs)/etc/permissions/com.google.android.maps.xml \
    vendor/pixelgapps/etc/preferred-apps/google.xml:$(copyoutblobs)/etc/preferred-apps/google.xml \
    vendor/pixelgapps/etc/sysconfig/google.xml:$(copyoutblobs)/etc/sysconfig/google.xml \
    vendor/pixelgapps/etc/sysconfig/google_build.xml:$(copyoutblobs)/etc/sysconfig/google_build.xml \
    vendor/pixelgapps/etc/sysconfig/google-hiddenapi-package-whitelist.xml:$(copyoutblobs)/etc/sysconfig/google-hiddenapi-package-whitelist.xml \
    vendor/pixelgapps/etc/sysconfig/google_vr_build.xml:$(copyoutblobs)/etc/sysconfig/google_vr_build.xml \
    vendor/pixelgapps/etc/sysconfig/nexus.xml:$(copyoutblobs)/etc/sysconfig/nexus.xml \
    vendor/pixelgapps/etc/sysconfig/pixel_experience_2017.xml:$(copyoutblobs)/etc/sysconfig/pixel_experience_2017.xml \
    vendor/pixelgapps/etc/sysconfig/pixel_experience_2018.xml:$(copyoutblobs)/etc/sysconfig/pixel_experience_2018.xml \
    vendor/pixelgapps/etc/sysconfig/qti_whitelist.xml:$(copyoutblobs)/etc/sysconfig/qti_whitelist.xml \
    vendor/pixelgapps/etc/sysconfig/whitelist_com.android.omadm.service.xml:$(copyoutblobs)/etc/sysconfig/whitelist_com.android.omadm.service.xml \
    vendor/pixelgapps/framework/org.apache.http.legacy.jar:$(copyoutblobs)/framework/org.apache.http.legacy.jar \
    vendor/pixelgapps/framework/org.apache.http.legacy.jar.prof:$(copyoutblobs)/framework/org.apache.http.legacy.jar.prof \
    vendor/pixelgapps/lib/libfilterpack_imageproc.so:$(copyoutblobs)/lib/libfilterpack_imageproc.so \
    vendor/pixelgapps/lib64/libfilterpack_imageproc.so:$(copyoutblobs)/lib64/libfilterpack_imageproc.so \
    vendor/pixelgapps/lib64/libjni_latinimegoogle.so:$(copyoutblobs)/lib64/libjni_latinimegoogle.so \
    vendor/pixelgapps/lib64/libjni_latinime.so:$(copyoutblobs)/lib64/libjni_latinime.so \
    vendor/pixelgapps/lib64/libsketchology_native.so:$(copyoutblobs)/lib64/libsketchology_native.so \
    vendor/pixelgapps/lib64/libwallpapers-breel-2018-jni.so:$(copyoutblobs)/lib64/libwallpapers-breel-2018-jni.so \
    vendor/pixelgapps/app/CalculatorGooglePrebuilt/CalculatorGooglePrebuilt.apk.prof:$(copyoutblobs)/app/CalculatorGooglePrebuilt/CalculatorGooglePrebuilt.apk.prof \
    vendor/pixelgapps/app/GoogleCamera/GoogleCamera.apk.prof:$(copyoutblobs)/app/GoogleCamera/GoogleCamera.apk.prof \
    vendor/pixelgapps/app/GoogleContacts/GoogleContacts.apk.prof:$(copyoutblobs)/app/GoogleContacts/GoogleContacts.apk.prof \
    vendor/pixelgapps/app/PrebuiltDeskClockGoogle/PrebuiltDeskClockGoogle.apk.prof:$(copyoutblobs)/app/PrebuiltDeskClockGoogle/PrebuiltDeskClockGoogle.apk.prof \
    vendor/pixelgapps/priv-app/Velvet/lib/arm64/libccl.so:$(copyoutblobs)/priv-app/Velvet/lib/arm64/libccl.so \
    vendor/pixelgapps/priv-app/Velvet/lib/arm64/libcronet.61.0.3160.0.so:$(copyoutblobs)/priv-app/Velvet/lib/arm64/libcronet.61.0.3160.0.so \
    vendor/pixelgapps/priv-app/Velvet/lib/arm64/libframesequence.so:$(copyoutblobs)/priv-app/Velvet/lib/arm64/libframesequence.so \
    vendor/pixelgapps/priv-app/Velvet/lib/arm64/libgoogle_speech_jni.so:$(copyoutblobs)/priv-app/Velvet/lib/arm64/libgoogle_speech_jni.so \
    vendor/pixelgapps/priv-app/Velvet/lib/arm64/libgoogle_speech_micro_jni.so:$(copyoutblobs)/priv-app/Velvet/lib/arm64/libgoogle_speech_micro_jni.so \
    vendor/pixelgapps/priv-app/Velvet/lib/arm64/libnativecrashreporter.so:$(copyoutblobs)/priv-app/Velvet/lib/arm64/libnativecrashreporter.so \
    vendor/pixelgapps/priv-app/Velvet/lib/arm64/liboffline_actions_jni.so:$(copyoutblobs)/priv-app/Velvet/lib/arm64/liboffline_actions_jni.so \
    vendor/pixelgapps/priv-app/Velvet/lib/arm64/libhere_allpurpose.so:$(copyoutblobs)/priv-app/Velvet/lib/arm64/libhere_allpurpose.so \
    vendor/pixelgapps/usr/srec/en-US/am_phonemes.syms:$(copyoutblobs)/usr/srec/en-US/am_phonemes.syms \
    vendor/pixelgapps/usr/srec/en-US/app_bias.fst:$(copyoutblobs)/usr/srec/en-US/app_bias.fst \
    vendor/pixelgapps/usr/srec/en-US/APP_NAME.fst:$(copyoutblobs)/usr/srec/en-US/APP_NAME.fst \
    vendor/pixelgapps/usr/srec/en-US/APP_NAME.syms:$(copyoutblobs)/usr/srec/en-US/APP_NAME.syms \
    vendor/pixelgapps/usr/srec/en-US/c_fst:$(copyoutblobs)/usr/srec/en-US/c_fst \
    vendor/pixelgapps/usr/srec/en-US/CLG.prewalk.fst:$(copyoutblobs)/usr/srec/en-US/CLG.prewalk.fst \
    vendor/pixelgapps/usr/srec/en-US/commands.abnf:$(copyoutblobs)/usr/srec/en-US/commands.abnf \
    vendor/pixelgapps/usr/srec/en-US/compile_grammar.config:$(copyoutblobs)/usr/srec/en-US/compile_grammar.config \
    vendor/pixelgapps/usr/srec/en-US/config.pumpkin:$(copyoutblobs)/usr/srec/en-US/config.pumpkin \
    vendor/pixelgapps/usr/srec/en-US/confirmation_bias.fst:$(copyoutblobs)/usr/srec/en-US/confirmation_bias.fst \
    vendor/pixelgapps/usr/srec/en-US/CONTACT_NAME.fst:$(copyoutblobs)/usr/srec/en-US/CONTACT_NAME.fst \
    vendor/pixelgapps/usr/srec/en-US/CONTACT_NAME.syms:$(copyoutblobs)/usr/srec/en-US/CONTACT_NAME.syms \
    vendor/pixelgapps/usr/srec/en-US/contacts.abnf:$(copyoutblobs)/usr/srec/en-US/contacts.abnf \
    vendor/pixelgapps/usr/srec/en-US/contacts_bias.fst:$(copyoutblobs)/usr/srec/en-US/contacts_bias.fst \
    vendor/pixelgapps/usr/srec/en-US/contacts_disambig.fst:$(copyoutblobs)/usr/srec/en-US/contacts_disambig.fst \
    vendor/pixelgapps/usr/srec/en-US/dict:$(copyoutblobs)/usr/srec/en-US/dict \
    vendor/pixelgapps/usr/srec/en-US/dictation.config:$(copyoutblobs)/usr/srec/en-US/dictation.config \
    vendor/pixelgapps/usr/srec/en-US/dnn:$(copyoutblobs)/usr/srec/en-US/dnn \
    vendor/pixelgapps/usr/srec/en-US/embedded_class_denorm.mfar:$(copyoutblobs)/usr/srec/en-US/embedded_class_denorm.mfar \
    vendor/pixelgapps/usr/srec/en-US/embedded_normalizer.mfar:$(copyoutblobs)/usr/srec/en-US/embedded_normalizer.mfar \
    vendor/pixelgapps/usr/srec/en-US/endpointer_dictation.config:$(copyoutblobs)/usr/srec/en-US/endpointer_dictation.config \
    vendor/pixelgapps/usr/srec/en-US/endpointer_model:$(copyoutblobs)/usr/srec/en-US/endpointer_model \
    vendor/pixelgapps/usr/srec/en-US/endpointer_model.mmap:$(copyoutblobs)/usr/srec/en-US/endpointer_model.mmap \
    vendor/pixelgapps/usr/srec/en-US/endpointer_voicesearch.config:$(copyoutblobs)/usr/srec/en-US/endpointer_voicesearch.config \
    vendor/pixelgapps/usr/srec/en-US/ep_portable_mean_stddev:$(copyoutblobs)/usr/srec/en-US/ep_portable_mean_stddev \
    vendor/pixelgapps/usr/srec/en-US/ep_portable_model.uint8.mmap:$(copyoutblobs)/usr/srec/en-US/ep_portable_model.uint8.mmap \
    vendor/pixelgapps/usr/srec/en-US/g2p.data:$(copyoutblobs)/usr/srec/en-US/g2p.data \
    vendor/pixelgapps/usr/srec/en-US/g2p_fst:$(copyoutblobs)/usr/srec/en-US/g2p_fst \
    vendor/pixelgapps/usr/srec/en-US/g2p_graphemes.syms:$(copyoutblobs)/usr/srec/en-US/g2p_graphemes.syms \
    vendor/pixelgapps/usr/srec/en-US/g2p_phonemes.syms:$(copyoutblobs)/usr/srec/en-US/g2p_phonemes.syms \
    vendor/pixelgapps/usr/srec/en-US/grammar.config:$(copyoutblobs)/usr/srec/en-US/grammar.config \
    vendor/pixelgapps/usr/srec/en-US/hmmlist:$(copyoutblobs)/usr/srec/en-US/hmmlist \
    vendor/pixelgapps/usr/srec/en-US/hmm_symbols:$(copyoutblobs)/usr/srec/en-US/hmm_symbols \
    vendor/pixelgapps/usr/srec/en-US/input_mean_std_dev:$(copyoutblobs)/usr/srec/en-US/input_mean_std_dev \
    vendor/pixelgapps/usr/srec/en-US/lexicon.U.fst:$(copyoutblobs)/usr/srec/en-US/lexicon.U.fst \
    vendor/pixelgapps/usr/srec/en-US/lstm_model.uint8.data:$(copyoutblobs)/usr/srec/en-US/lstm_model.uint8.data \
    vendor/pixelgapps/usr/srec/en-US/magic_mic.config:$(copyoutblobs)/usr/srec/en-US/magic_mic.config \
    vendor/pixelgapps/usr/srec/en-US/media_bias.fst:$(copyoutblobs)/usr/srec/en-US/media_bias.fst \
    vendor/pixelgapps/usr/srec/en-US/metadata:$(copyoutblobs)/usr/srec/en-US/metadata \
    vendor/pixelgapps/usr/srec/en-US/monastery_config.pumpkin:$(copyoutblobs)/usr/srec/en-US/monastery_config.pumpkin \
    vendor/pixelgapps/usr/srec/en-US/norm_fst:$(copyoutblobs)/usr/srec/en-US/norm_fst \
    vendor/pixelgapps/usr/srec/en-US/offensive_word_normalizer.mfar:$(copyoutblobs)/usr/srec/en-US/offensive_word_normalizer.mfar \
    vendor/pixelgapps/usr/srec/en-US/offline_action_data.pb:$(copyoutblobs)/usr/srec/en-US/offline_action_data.pb \
    vendor/pixelgapps/usr/srec/en-US/phonelist:$(copyoutblobs)/usr/srec/en-US/phonelist \
    vendor/pixelgapps/usr/srec/en-US/portable_lstm:$(copyoutblobs)/usr/srec/en-US/portable_lstm \
    vendor/pixelgapps/usr/srec/en-US/portable_meanstddev:$(copyoutblobs)/usr/srec/en-US/portable_meanstddev \
    vendor/pixelgapps/usr/srec/en-US/pumpkin.mmap:$(copyoutblobs)/usr/srec/en-US/pumpkin.mmap \
    vendor/pixelgapps/usr/srec/en-US/read_items_bias.fst:$(copyoutblobs)/usr/srec/en-US/read_items_bias.fst \
    vendor/pixelgapps/usr/srec/en-US/rescoring.fst.compact:$(copyoutblobs)/usr/srec/en-US/rescoring.fst.compact \
    vendor/pixelgapps/usr/srec/en-US/semantics.pumpkin:$(copyoutblobs)/usr/srec/en-US/semantics.pumpkin \
    vendor/pixelgapps/usr/srec/en-US/skip_items_bias.fst:$(copyoutblobs)/usr/srec/en-US/skip_items_bias.fst \
    vendor/pixelgapps/usr/srec/en-US/SONG_NAME.fst:$(copyoutblobs)/usr/srec/en-US/SONG_NAME.fst \
    vendor/pixelgapps/usr/srec/en-US/SONG_NAME.syms:$(copyoutblobs)/usr/srec/en-US/SONG_NAME.syms \
    vendor/pixelgapps/usr/srec/en-US/time_bias.fst:$(copyoutblobs)/usr/srec/en-US/time_bias.fst \
    vendor/pixelgapps/usr/srec/en-US/transform.mfar:$(copyoutblobs)/usr/srec/en-US/transform.mfar \
    vendor/pixelgapps/usr/srec/en-US/voice_actions.config:$(copyoutblobs)/usr/srec/en-US/voice_actions.config \
    vendor/pixelgapps/usr/srec/en-US/voice_actions_compiler.config:$(copyoutblobs)/usr/srec/en-US/voice_actions_compiler.config \
    vendor/pixelgapps/usr/srec/en-US/word_confidence_classifier:$(copyoutblobs)/usr/srec/en-US/word_confidence_classifier \
    vendor/pixelgapps/usr/srec/en-US/wordlist.syms:$(copyoutblobs)/usr/srec/en-US/wordlist.syms
