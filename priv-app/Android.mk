# Copyright (C) 2017 The Pure Nexus Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := ConfigUpdater
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := ConfigUpdater/ConfigUpdater.apk
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := APPS
LOCAL_PRIVILEGED_MODULE := true
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := $(product_bool)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := ConnMetrics
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := ConnMetrics/ConnMetrics.apk
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := APPS
LOCAL_PRIVILEGED_MODULE := true
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := $(product_bool)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := GoogleDialer
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := GoogleDialer/GoogleDialer.apk
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := APPS
LOCAL_OVERRIDES_PACKAGES := Dialer
LOCAL_PRIVILEGED_MODULE := true
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := $(product_bool)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := GoogleExtServicesPrebuilt
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := GoogleExtServicesPrebuilt/GoogleExtServicesPrebuilt.apk
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := APPS
LOCAL_OVERRIDES_PACKAGES := ExtServices
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := $(product_bool)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := GoogleFeedback
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := GoogleFeedback/GoogleFeedback.apk
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := APPS
LOCAL_PRIVILEGED_MODULE := true
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := $(product_bool)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := AndroidMigratePrebuilt
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := AndroidMigratePrebuilt/AndroidMigratePrebuilt.apk
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := APPS
LOCAL_PRIVILEGED_MODULE := true
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := $(product_bool)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := GoogleOneTimeInitializer
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := GoogleOneTimeInitializer/GoogleOneTimeInitializer.apk
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := APPS
LOCAL_PRIVILEGED_MODULE := true
LOCAL_OVERRIDES_PACKAGES := OneTimeInitializer
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := $(product_bool)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := GooglePartnerSetup
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := GooglePartnerSetup/GooglePartnerSetup.apk
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := APPS
LOCAL_PRIVILEGED_MODULE := true
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := $(product_bool)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := GoogleServicesFramework
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := GoogleServicesFramework/GoogleServicesFramework.apk
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := APPS
LOCAL_PRIVILEGED_MODULE := true
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := $(product_bool)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := OTAConfigPrebuilt
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := $(LOCAL_MODULE)/$(LOCAL_MODULE).apk
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := APPS
LOCAL_PRIVILEGED_MODULE := true
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := $(product_bool)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := Phonesky
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := Phonesky/Phonesky.apk
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := APPS
LOCAL_PRIVILEGED_MODULE := true
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := $(product_bool)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := PrebuiltGmsCore
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := PrebuiltGmsCore/PrebuiltGmsCoreQt.apk
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := APPS
LOCAL_PRIVILEGED_MODULE := true
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := $(product_bool)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := DynamiteModulesA
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreQt_DynamiteModulesA.apk
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := m
LOCAL_PRIVILEGED_MODULE := true
LOCAL_MODULE_PATH := $(copyoutblobs)/priv-app/PrebuiltGmsCore/app_chimera/m/
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := $(product_bool)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := AdsDynamite
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreQt_AdsDynamite.apk
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := m
LOCAL_PRIVILEGED_MODULE := true
LOCAL_MODULE_PATH := $(copyoutblobs)/priv-app/PrebuiltGmsCore/app_chimera/m/
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := $(product_bool)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := DynamiteModulesC
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreQt_DynamiteModulesC.apk
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := m
LOCAL_PRIVILEGED_MODULE := true
LOCAL_MODULE_PATH := $(copyoutblobs)/priv-app/PrebuiltGmsCore/app_chimera/m/
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := $(product_bool)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := CronetDynamite
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreQt_CronetDynamite.apk
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := m
LOCAL_PRIVILEGED_MODULE := true
LOCAL_MODULE_PATH := $(copyoutblobs)/priv-app/PrebuiltGmsCore/app_chimera/m/
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := $(product_bool)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := GoogleCertificates
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreQt_GoogleCertificates.apk
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := m
LOCAL_PRIVILEGED_MODULE := true
LOCAL_MODULE_PATH := $(copyoutblobs)/priv-app/PrebuiltGmsCore/app_chimera/m/
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := $(product_bool)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := DynamiteLoader
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreQt_DynamiteLoader.apk
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := m
LOCAL_PRIVILEGED_MODULE := true
LOCAL_MODULE_PATH := $(copyoutblobs)/priv-app/PrebuiltGmsCore/app_chimera/m/
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := $(product_bool)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := MapsDynamite
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreQt_MapsDynamite.apk
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := m
LOCAL_PRIVILEGED_MODULE := true
LOCAL_MODULE_PATH := $(copyoutblobs)/priv-app/PrebuiltGmsCore/app_chimera/m/
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := $(product_bool)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := MatchmakerPrebuilt
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := MatchmakerPrebuilt/MatchmakerPrebuilt.apk
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := APPS
LOCAL_PRIVILEGED_MODULE := true
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := $(product_bool)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := MeasurementDynamite
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := PrebuiltGmsCore/app_chimera/m/PrebuiltGmsCoreQt_MeasurementDynamite.apk
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := m
LOCAL_PRIVILEGED_MODULE := true
LOCAL_MODULE_PATH := $(copyoutblobs)/priv-app/PrebuiltGmsCore/app_chimera/m/
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := $(product_bool)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := PixelSetupWizard
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := PixelSetupWizard/PixelSetupWizard.apk
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := APPS
LOCAL_PRIVILEGED_MODULE := true
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := $(product_bool)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := SetupWizardPrebuilt
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := SetupWizardPrebuilt/SetupWizardPrebuilt.apk
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := APPS
LOCAL_PRIVILEGED_MODULE := true
LOCAL_OVERRIDES_PACKAGES := Provision
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := $(product_bool)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := TurboPrebuilt
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := TurboPrebuilt/TurboPrebuilt.apk
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := APPS
LOCAL_PRIVILEGED_MODULE := true
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := $(product_bool)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE := Velvet
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := Velvet/Velvet.apk
LOCAL_OVERRIDES_PACKAGES := QuickSearchBox
LOCAL_CERTIFICATE := PRESIGNED
LOCAL_MODULE_CLASS := APPS
LOCAL_PRIVILEGED_MODULE := true
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRODUCT_MODULE := $(product_bool)
include $(BUILD_PREBUILT)
