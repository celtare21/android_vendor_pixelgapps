# Copyright (C) 2017 The Pure Nexus Project
# Copyright (C) 2018-2019 The Dirty Unicorns Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

ifneq ($(filter product system/product,$(TARGET_COPY_OUT_PRODUCT)),)
  TARGET_COPY_GAPPS_OUT := $(get_build_var TARGET_COPY_OUT_SYSTEM)
  product_bool := false
  copyoutblobs := system
else
  TARGET_COPY_GAPPS_OUT := $(get_build_var TARGET_COPY_OUT_PRODUCT)
  product_bool := true
  copyoutblobs := product
endif

# App
PRODUCT_PACKAGES += \
    CalculatorGooglePrebuilt \
    CalendarGooglePrebuilt \
    GoogleCamera \
    GoogleContacts \
    GoogleContactsSyncAdapter \
    GoogleTTS \
    GoogleVrCore \
    LatinIMEGooglePrebuilt \
    MarkupGoogle \
    Photos \
    PrebuiltBugle \
    PrebuiltDeskClockGoogle \
    talkback \
    WallpapersBReel18

# Priv-app
PRODUCT_PACKAGES += \
    AdsDynamite \
    AndroidMigratePrebuilt \
    CarrierSetup \
    ConfigUpdater \
    ConnMetrics \
    CronetDynamite \
    DynamiteLoader \
    DynamiteModulesA \
    DynamiteModulesC \
    GoogleCertificates \
    GoogleDialer \
    GoogleExtServicesPrebuilt \
    GoogleFeedback \
    GoogleOneTimeInitializer \
    GooglePartnerSetup \
    GoogleServicesFramework \
    MapsDynamite \
    MatchmakerPrebuilt \
    OTAConfigPrebuilt \
    Phonesky \
    PixelSetupWizard \
    PrebuiltGmsCore \
    SetupWizardPrebuilt \
    TurboPrebuilt \
    Velvet

# Framework
PRODUCT_PACKAGES += \
    com.google.android.maps \
    com.google.android.dialer.support

# Overlays
PRODUCT_PACKAGE_OVERLAYS += \
    vendor/pixelgapps/overlay/

# Enable Google Assistant
PRODUCT_SYSTEM_DEFAULT_PROPERTIES += \
    ro.opa.eligible_device=true

$(call inherit-product, vendor/pixelgapps/pixel-gapps-blobs.mk)
